<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Sumbangan;
class SumbanganController extends Controller
{

    public function index()
    {
        return view('sumbangan.index');
    }

    public function create()
    {
        return view('sumbangan.masukkansumbangan');
    }

    public function tampilanSumbangan()
    {
        return view('sumbangan.tampilansumbangan');
    }

    public function send()
    {
        return view('sumbangan.kirimsumbangan');
    }

    public function login()
    {
        return view('sumbangan.login');
    }

    public function reg()
    {
        return view('sumbangan.register');
    }

    public function welcome()
    {
        return view('welcome') ;
    }

    public function dataSumbangan()
    {
        $data = Sumbangan::query();
        return Datatables::of($data)->make(true);
    }

    
}

