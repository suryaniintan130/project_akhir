<!DOCTYPE html>
<html>
<head>
<title>We Do</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" shrink-to-fit="no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- favicons -->
<link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
<!-- favicons -->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/custom-responsive-style.css">
<script type="text/javascript" src="scripts/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="scripts/plugin-active.js"></script>
</head>
<body>
  <header>
    <div class="mobile-logo">
    <a href=""><img src="images/ds-logo.png"></a>
  </div>
  <a class="menu-toggle rounded" href="#">
    <i class="fa fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="smooth-scroll" href="#Header"></a>
      </li>
      <li class="sidebar-nav-item">
        <a class="smooth-scroll" href="#Services">Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="nav-link" href="/login">Login</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="nav-link" href="/register">Register</a>
      </li>
    </ul>
  </nav>
  </header>
<section id="Banner">
  <div class="logo">
    <img src="images/ds-logo.png">
  </div>
  <div class="blacksection">
    <h1>Permudah<br>Manajemen<br>Sumbangan</h1>
  </div>
</section>
<a href="#Services" class="mscroll"><img src="images/mouse-icon.png" alt="mouse icon"></a>
<section id="Services">
  <div class="container">
    <h2>Permudah Manajemen Sumbangan</h2>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <div class="each-services">
          <img src="images/digital-marketing.jpg" alt="Services">
          <h3>Mendata Penerimaan Sumbangan</h3>
          <p>Pada fitur ini digunakan untuk mendata setiap data sumbangan yang masuk</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <div class="each-services">
          <img src="images/web-hosting.jpg" alt="Services">
          <h3>Menampilkan Data Sumbangan</h3>
          <p>Pada fitur ini menampilkan data sumbangan yang telah disimpan</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <div class="each-services">
          <img src="images/web-design.jpg" alt="Services">
          <h3>Pengiriman Sumbangan</h3>
          <p> Pada fitur ini untuk mengurangi data pada sumbangan yang telah disimpan karena adanya pengiriman sumbangan</p>
        </div>
      </div>
    </div>
  </div>
</section>

 <div class="copyright"><p>Projek Akhir Cahyo & Intan</p>
</div>
</body>
</html>