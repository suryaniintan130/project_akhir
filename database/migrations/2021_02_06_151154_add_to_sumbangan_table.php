<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToSumbanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sumbangan', function (Blueprint $table) {
            $table->unsignedBigInteger('profile_id');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->unsignedBigInteger('masuk_id');
            $table->foreign('masuk_id')->references('id')->on('masuk');
            $table->unsignedBigInteger('keluar_id');
            $table->foreign('keluar_id')->references('id')->on('keluar');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sumbangan', function (Blueprint $table) {
            //
        });
    }
}
